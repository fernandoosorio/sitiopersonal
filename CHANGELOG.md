# Changelog
Cambios realizados por versión de mi web personal.

Formato basado en [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
al mismo tiempo, adherido a [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [7.2.2] - 2018-04-09
### Modificado
- Cambio de URL hacia la API.
- Reducción de dependencias production.

## [7.2.1] - 2018-03-04
### Modificado
- Cambio de URLs de bitbucket.
- Correcciones en README y CHANGELOG.

## [7.2.0] - 2021-03-03
### Nuevo
- Cambio de paleta de colores.
- Cambio de forma de incorporación de Bootstrap.
- Upgrade de Bootstrap a v5-beta2.
- Remoción de código duplicado.
- Cambios adicionales en las vistas.

## [7.1.0] - 2021-01-22
### Nuevo
- Upgrade de Angular, de 10 a 11.
- Reemplazo de requests, de subscribes a observables.
- Cambio de marca, de Fermoto5HD a FernandoOsor.io

## [7.0.0] - 2020-07-25
### Nuevo
- Upgrade de Angular, de 7 a 10.
- Upgrade de Bootstrap v4 a v5-alpha1.
- Implementación completa de SEO.
- Fade in/out y animaciones en la navegación de la página.
- Template actualizado.
- Menú mobile accesible desde abajo.
- Vistas individuales para los trabajos realizados.

## [6.5.0] - 2018-12-06
### Nuevo
- Upgrade de Angular, de 5 a 7.
- Integración de Angular Universal + SSR. Primera iteración de SEO.

## [6.3.0] - 2018-03-20
### Modificado
- Separación de API - El repositorio sólo contendrá la página en forma estática mientras que la API quedará en un repositorio privado (por obvias razones).
- Cover landing de mayo.

### Nuevo
- Portfolio: Separación por categorías.
- Portfolio: Página simple para cada trabajo realizado.
- DollarsCP: Más funcionalidades agregadas - Aún queda más por agregar.

## [6.2.0] - 2018-03-20
### Modificado
- FM5UX v3.1
- Home: Cover y disposición de redes.
- Modificaciones en IAS.
- Eliminación de métodos inutilizados.

### Nuevo
- Integración de backend con PassportJS.
- Contacto sin mailing (To-do: Configurar Docker para el envío de mailing).

## [6.1.1] - 2017-12-23
### Modificado
- Corrección de link de changelog.

## [6.1.0] - 2017-12-23
### Nuevo
- APIs de Facebook y Twitter (no integrados aún en el sitio).
- Sistema de ads internos (IAS - Internal Ad Serving - no son bloqueados por adblockers).
- Reestructuración de assets (static y dist). 
- Sección OpenSource.

### Modificado
- Base de Angular 4 a Angular 5. 
- FontAwesome 4 a FontAwesome 5 Pro.
- Renombre de framework frontend (FM5strap a FM5UX). 
- Footer (aún no integrado en FM5UX). 
- Jumbotrons son componentes dinámicos (la mayoría fueron traspasados a este formato). 
- Optimización de imágenes.

## [6.0.0] - 2017-12-03
### Nuevo
- Todo el sitio - First commit!
- Archivo CHANGELOG.md (este).

[Unreleased]: https://github.com/Fermoto5HD/sitiopersonal/compare/v7.2.2...HEAD
[7.2.1]: https://github.com/Fermoto5HD/sitiopersonal/compare/v7.2.1...v7.2.2
[7.2.0]: https://github.com/Fermoto5HD/sitiopersonal/compare/v7.2.0...v7.2.1
[7.1.0]: https://github.com/Fermoto5HD/sitiopersonal/compare/v7.1.0...v7.2.0
[7.0.0]: https://github.com/Fermoto5HD/sitiopersonal/compare/v7.0.0...v7.1.0
[6.5.0]: https://github.com/Fermoto5HD/sitiopersonal/compare/v6.5.0...v7.0.0
[6.3.0]: https://github.com/Fermoto5HD/sitiopersonal/compare/v6.3.0...v6.5.0
[6.2.0]: https://github.com/Fermoto5HD/sitiopersonal/compare/v6.2.0...v6.3.0
[6.1.1]: https://github.com/Fermoto5HD/sitiopersonal/compare/v6.1.1...v6.2.0
[6.1.1]: https://github.com/Fermoto5HD/sitiopersonal/compare/v6.1.0...v6.1.1
[6.1.0]: https://github.com/Fermoto5HD/sitiopersonal/compare/v6.0.0...v6.1.0