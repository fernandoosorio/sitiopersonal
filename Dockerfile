FROM node:alpine

COPY . /usr/src/sitiopersonal
WORKDIR /usr/src/sitiopersonal

RUN npm cache clean && npm install --only=prod
ENV NODE_ENV production

ENV PORT 3000

EXPOSE 3000

CMD ["npm", "start"]