import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DollarsLoginComponent } from './components/login/section.component';

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'login',
		component: DollarsLoginComponent
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DollarsRoutingModule { }
