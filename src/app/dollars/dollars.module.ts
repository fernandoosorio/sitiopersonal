import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DollarsRoutingModule } from './dollars-routing.module';
import { DollarsLoginComponent } from './components/login/section.component';

@NgModule({
  declarations: [
  	DollarsLoginComponent
  ],
  imports: [
    CommonModule,
	FormsModule,
	ReactiveFormsModule,
    DollarsRoutingModule
  ]
})
export class DollarsModule { }
