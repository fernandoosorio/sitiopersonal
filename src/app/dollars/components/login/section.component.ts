import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import { FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ApiService } from '@services/api.service';
import { NotificationService } from '@services/notification.service';

@Component({
  selector: 'dollars-login',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class DollarsLoginComponent implements OnInit {

	public btn:any = {
		current: {
			text: 'Loading...',
			disabled: true
		},
		states: {
			ready: {
				text: 'Enter',
				disabled: false
			},
			loading: {
				text: 'Please wait...',
				disabled: true
			},
			done: {
				text: 'Welcome!',
				disabled: true
			}
		}
	}

	constructor(
		meta: Meta,
		title: Title,
		private apiService: ApiService,
		private notificationService: NotificationService,
		private fb: FormBuilder
	) {
		title.setTitle('Dollars');
	}
	
	public main_form = this.fb.group({
		email: ['', [Validators.required, Validators.email]],
		password: ['', [Validators.required, Validators.minLength(8)]]
	});

	ngOnInit() {
		this.btn.current = this.btn.states.ready;

		let that = this;
		setTimeout(function() {
			that.notificationService.trigger({
				title: '¡Atención!',
				text: 'Esta sección no tiene funcionalidad alguna.'
			});
		}, 1000);
	}

	public onSubmit() {
		console.log('Submit!');
	}
}
