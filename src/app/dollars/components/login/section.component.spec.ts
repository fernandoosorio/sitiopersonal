import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DollarsLoginComponent } from './section.component';

describe('DollarsLoginComponent', () => {
  let component: DollarsLoginComponent;
  let fixture: ComponentFixture<DollarsLoginComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DollarsLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DollarsLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
