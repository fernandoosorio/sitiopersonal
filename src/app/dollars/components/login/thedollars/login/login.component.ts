import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Meta, Title } from "@angular/platform-browser";
import { IkebukuroService } from '../../../services/ikebukuro.service';

@Component({
	selector: 'fm5-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;
	loading = false;
	submitted = false;
	next: string;
	available = null;

	constructor(
		meta: Meta,
		title: Title,
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private IkebukuroService: IkebukuroService
	) {
		title.setTitle('The Dollars');
		meta.addTags([
			{ name: 'robots', content: 'NOINDEX, NOFOLLOW' },
			{ name: 'description', content: 'This is not a login link. Not at all.' },
			{ name: 'og:title', content: 'Not a login link. Trust me.' },
			{ name: 'og:description', content: 'This is not a login link. Not at all.' },
			{ name: 'twitter:title', content: 'Not a login link. Trust me.' },
			{ name: 'twitter:description', content: 'This is not a login link. Not at all.' }
		]);
		this.IkebukuroService.check_server();
		this.IkebukuroService.available.subscribe((res) => {
			this.available = res;
		});
		console.log(this.available);
	}

	ngOnInit() {
		this.loginForm = this.formBuilder.group({
			username: ['', Validators.required],
			passcode: ['', Validators.required]
		});

		// get return url from route parameters or default to '/'
		this.next = this.route.snapshot.queryParams['next'] || '/';
	}

	get f() {
		return this.loginForm.controls;
	}

	onSubmit() {
		this.submitted = true;

		// stop here if form is invalid
		if (this.loginForm.invalid) {
			return;
		}

		this.loading = true;
		this.IkebukuroService.login(this.f.username.value, this.f.passcode.value)
			.pipe(first())
			.subscribe(
				data => {
					this.router.navigate([this.next]);
				},
				error => {
					console.log(error);
					this.loading = false;
				});
	}
}
