import {
  animation, trigger, animateChild, group,
  transition, animate, style, query
} from '@angular/animations';

export const slideInAnimation =
	trigger('routeAnimations', [
		transition('* <=> *', [
			style({
				position: 'relative',
				height: '200vh',

				transitionDuration: '0.25s'
			}),
			query(':enter, :leave', [
				style({
					bottom: 0,
					left: 0,
					height: '200vh',
					position: 'absolute',
					top: 0,
					transitionDuration: '0.25s',
					width: '100%',
					zIndex: 1000
				})
			], { optional: true }),
			group([
				query(':leave', [
					animate('0ms ease-out', style({ opacity: 1})),
					animate('125ms ease-out', style({ opacity: 0})),
					animate('125ms ease-out', style({ opacity: 0}))
				], { optional: true }),
				query(':enter', [
					animate('0ms ease-out', style({ opacity: 0})),
					animate('125ms ease-out', style({ opacity: 0})),
					animate('125ms ease-out', style({ opacity: 1}))
				], { optional: true })
			]),
			query(':enter', animateChild(), { optional: true }),
		])
	]);