import { BrowserModule, TransferState } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TransferHttpCacheModule } from '@nguniversal/common';
import { translateBrowserLoaderFactory } from './loaders/translate-browser.loader';

import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { OpenSourceComponent } from './components/opensource/landing.component';
import { JumbotronComponent } from './components/jumbotron/jumbotron.component';
import { Error404Component } from './components/error404/landing.component';
import { PortfolioLandingComponent } from './components/portfolio/landing/landing.component';
import { PortfolioItemsComponent } from './components/portfolio/items/items.component';
import { PortfolioWorkComponent } from './components/portfolio/work/work.component';
import { BlogLandingComponent } from './components/blog/landing/landing.component';
import { BlogPostComponent } from './components/blog/post/post.component';

// Services
import { GlobalService } from './services/global.service';

// Guards
import { BlogGuard } from './guards/blog.guard';
import { PortfolioGuard } from './guards/portfolio.guard';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    OpenSourceComponent,
    JumbotronComponent,
    Error404Component,
    PortfolioLandingComponent,
    PortfolioItemsComponent,
    PortfolioWorkComponent,
    BlogLandingComponent,
    BlogPostComponent
  ],
  imports:[
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    TransferHttpCacheModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateBrowserLoaderFactory,
        deps: [HttpClient, TransferState]
      }
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    GlobalService,
    BlogGuard,
    PortfolioGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
