import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';

// Components
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { OpenSourceComponent } from './components/opensource/landing.component';
import { PortfolioLandingComponent } from './components/portfolio/landing/landing.component';
import { PortfolioWorkComponent } from './components/portfolio/work/work.component';
import { Error404Component } from './components/error404/landing.component';
import { BlogLandingComponent } from './components/blog/landing/landing.component';
import { BlogPostComponent } from './components/blog/post/post.component';

// Guards
import { BlogGuard } from './guards/blog.guard';
import { PortfolioGuard } from './guards/portfolio.guard';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent,
		pathMatch: 'full',
		data: {animation: 'HomePage'}
	},
	{
		path: 'home',
		component: HomeComponent,
		data: {animation: 'HomePage'}
	},
	{
		path: 'about',
		component: AboutComponent,
		data: {animation: 'AboutPage'}
	},
	{
		path: 'portfolio',
		component: PortfolioLandingComponent,
		data: {animation: 'PortfolioPage'}
	},
	{
		path: 'portfolio/:slug',
		component: PortfolioWorkComponent,
		canActivate: [PortfolioGuard],
		data: {animation: 'PortfolioWorkPage'}
	},
	{
		path: 'blog',
		component: BlogLandingComponent,
		data: {animation: 'BlogPage'}
	},
	{
		path: 'blog/:slug',
		component: BlogPostComponent,
		canActivate: [BlogGuard],
		data: {animation: 'BlogArticlePage'}
	},
	{
		path: 'opensource',
		component: OpenSourceComponent,
		data: {animation: 'OpensourcePage'}
	},
	{
    	path: 'dollars',
		loadChildren: () => import('./dollars/dollars.module').then(m => m.DollarsModule)
	},
	{
		path: '**',
		component: Error404Component,
	}
];

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', initialNavigation: 'enabled', relativeLinkResolution: 'legacy' })
	],
	exports: [RouterModule]
})

export class AppRoutingModule { }
