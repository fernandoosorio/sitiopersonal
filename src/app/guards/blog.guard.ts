import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ApiService } from '@services/api.service';

@Injectable()
export class BlogGuard implements CanActivate {
	
	constructor(
		private router: Router,
		private apiService: ApiService
	) {
	}

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		return this.apiService.get<boolean>('blog', {slug: next.params.slug})
      .pipe(
        map(res => {
          return true;
        }),
        catchError((err) => {
          this.router.navigate(['/not-found'], {
            skipLocationChange: true, // minimal effect. see https://github.com/angular/angular/issues/17004
            queryParams: {
              url: state.url
            }
          });
          return of(false);
        })
      );
	}
}
