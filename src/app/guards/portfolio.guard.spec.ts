import { TestBed, inject, waitForAsync } from '@angular/core/testing';

import { PortfolioGuard } from './blog.guard';

describe('PortfolioGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PortfolioGuard]
    });
  });

  it('should ...', inject([PortfolioGuard], (guard: PortfolioGuard) => {
    expect(guard).toBeTruthy();
  }));
});
