import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '@environments/environment';


@Injectable({
  providedIn: 'root'
})
export class IkebukuroService {

	constructor(
		private http: HttpClient
	) {}

	user:Subject<Object> = new Subject();
	available:Subject<Object> = new Subject();

	check_server() {
		console.log('check server');
		return this.http.get(environment.apiUri + 'ping')
			.pipe(
				map(res => {
					this.available.next(true);
					return res;
				}),
				catchError(err => {
					this.available.next(false);
					return err;
				})
			).subscribe(
				res => console.log(res),
				err => console.error(err),
				() => console.log("Processing Complete.")
			);
	}

	check_login() {
		/*this.user.next({data: {name: 'Fernando'}});*/
		/*let req = this.http.get(environment.apiUri + 'thedollars/ikebukuro')
		req.subscribe((res) => {
			this.user.next(res);
			console.log(this.user);
		});*/
	}

	login(username: string, passcode: string) {
		this.user.next({data: {name: 'Test'}});
		return this.http.post<any>(environment.apiUri + 'thedollars/login', { username, passcode })
			.pipe(map(res => {
				// login successful if there's a jwt token in the response
				if (res && res.token) {
					// store user details and jwt token in local storage to keep user logged in between page refreshes
					localStorage.setItem('ikebukuro_citizen', JSON.stringify(res));
					/*this.user.next(user);*/
					this.user.next(res);
				}
				return res;
			}));
	}

	logout() {
		this.user.next(null);
		return this.http.get(environment.apiUri + 'thedollars/logout')
			.pipe(map(res => {
				console.log(res);
				this.user.next(null);
				localStorage.removeItem('ikebukuro_citizen');
				return res;
			}));
	}
}
