import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
	providedIn: 'root'
})

export class GlobalService {

	lang: string = 'es';

	langChange: Subject<any> = new Subject<any>();

	constructor(
		private translate: TranslateService
	) {
		this.langChange.subscribe((lang) => {
			this.lang = lang;
			this.translate.use(this.lang);
		});
	}

	public changeLang(code) {
		this.langChange.next(code);
	}
}