import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

import { Observable, of, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private httpClient: HttpClient) {}

  ping$(): Observable<any> {
    console.log(environment.apiUri);
    return this.httpClient.get(`${environment.apiUri}/api/external`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log('Error:', message);
  }

  public get<T>(endpoint: string, params: any = null): Observable<T> {
    return this.httpClient
      .get<T>(environment.apiUri + endpoint, { params: params })
      .pipe(
        catchError((err) => {
          console.log('Handling error locally and rethrowing it...', err);
          return throwError(err);
        })
      );
  }

  public portfolio(params: any = null) {
    return this.httpClient.get(environment.apiUri + 'portfolio', {params: params});
  }
  public portfolioCategory(slug: string) {
    return this.httpClient.get(environment.apiUri + 'portfolio/category/' + slug);
  }
  public portfolioItem(slug: string, version: string = null) {
    return this.httpClient.get(environment.apiUri + 'portfolio/work/' + slug + '/' + version);
  }

  public getCuriousCat(endpoint) {
    return this.httpClient.get(environment.apiUri + endpoint);
  }
}