import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import { Notification } from '@models/notification.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public notification:Notification;
  public notificationObs: Subject<any> = new Subject<any>();

  constructor(
    private readonly zone: NgZone
  ) {
    /*this.notificationObs.subscribe((data:Notification) => {
      this.notification = data;
    });*/
  }

  public trigger(data:Notification): void {
    this.notificationObs.next(data);
  }
}
