import { TestBed } from '@angular/core/testing';

import { IkebukuroService } from './ikebukuro.service';

describe('IkebukuroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IkebukuroService = TestBed.get(IkebukuroService);
    expect(service).toBeTruthy();
  });
});
