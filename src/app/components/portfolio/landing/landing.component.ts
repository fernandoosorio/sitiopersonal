import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import { Observable } from 'rxjs';
import { startWith, delay, finalize, take, tap, switchMap } from 'rxjs/operators';
import { Portfolio } from '@models/portfolio.model';
import { ApiService } from '@services/api.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'portfolio-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class PortfolioLandingComponent implements OnInit {

  isLoading:boolean = true;
  public rows$: Observable<Portfolio[]>;

  onActivate(e, outlet){
    outlet.scrollTop = 0;
  }
  constructor(
    meta: Meta, 
    title: Title,
    private apiService:ApiService
   ) {
    title.setTitle('Portfolio - ' + environment.site_name);

    meta.removeTag('name="keywords"');
    meta.removeTag('name="description"');
    meta.removeTag('name="og:title"');
    meta.removeTag('name="og:description"');
    meta.removeTag('name="twitter:title"');
    meta.removeTag('name="twitter:description"');

    meta.addTags([
      { name: 'keywords', content: 'fernando, osorio, portfolio, frontend, buenos aires, argentina'},
      { name: 'description', content: 'Trabajos realizados durante mi carrera como desarrollador web.' },
      { name: 'og:title', content: 'Trabajos realizados' },
      { name: 'og:description', content: 'Trabajos realizados durante mi carrera como desarrollador web.' },
      { name: 'twitter:title', content: 'Trabajos realizados' },
      { name: 'twitter:description', content: 'Trabajos realizados durante mi carrera como desarrollador web.' }
    ]);
  }

  ngOnInit() {
    this.rows$ = this.apiService.get<Portfolio[]>('portfolio')
      .pipe(
        delay(0),
        tap((res:Portfolio[]) => {
          //console.log('pipe result', res);
        }),
        finalize(() => (this.isLoading = false))
      );
  }

}
