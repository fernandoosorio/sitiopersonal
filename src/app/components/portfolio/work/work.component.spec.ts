import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PortfolioWorkComponent } from './work.component';

describe('PortfolioWorkComponent', () => {
  let component: PortfolioWorkComponent;
  let fixture: ComponentFixture<PortfolioWorkComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
