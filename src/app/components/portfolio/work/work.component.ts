import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Meta, Title, DomSanitizer } from "@angular/platform-browser";
import { Observable } from 'rxjs';
import { startWith, delay, finalize, take, tap, switchMap } from 'rxjs/operators';
import { ApiService } from '@services/api.service';
import { Portfolio } from '@models/portfolio.model';
import * as moment from 'moment';
//import * as locales from 'moment/min/locales';
import { environment } from '@environments/environment';

@Component({
  selector: 'fm5-portfolio-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss']
})
export class PortfolioWorkComponent implements OnInit {
  slug;

  isLoading: boolean = true;
  public data$: Observable<Portfolio>;

  public version_tab = 0;

  constructor(
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,
    private meta: Meta, 
    private title: Title,
    private domsanitizer: DomSanitizer
  ) {
    
  }

  ngOnInit(): void {
    this.data$ = this.activatedRoute.params.pipe(
        switchMap((data) =>
          this.apiService
            .get<Portfolio>(`portfolio/work/` + data.slug)
            .pipe(
              delay(0),
              tap((data) => {
                if (data.versions) {
                  let versions = data.versions.reverse();
                  data.versions = versions;
                }

                if (data.name) {this.title.setTitle(data.name + ' - ' + environment.site_name);}
                this.meta.addTags([
                  { name: 'author',   content: 'Fernando Osorio'},
                  { name: 'keywords', content: 'fernando, osorio, portfolio, frontend, buenos aires, argentina'}
                ]);

                this.meta.removeTag('name="description"');
                this.meta.removeTag('name="og:title"');
                this.meta.removeTag('name="og:description"');
                this.meta.removeTag('name="og:image"');
                this.meta.removeTag('name="twitter:title"');
                this.meta.removeTag('name="twitter:description"');
                this.meta.removeTag('name="twitter:image"');

                if (data.excerpt) {this.meta.addTag({ name: 'description', content: data.excerpt });}

                if (data.name) {this.meta.addTag({ name: 'og:title', content: data.name });}
                if (data.excerpt) {this.meta.addTag({ name: 'og:description', content: data.excerpt });}
                if (data.image) {this.meta.addTag({ name: 'og:image', content: data.image });}

                if (data.name) {this.meta.addTag({ name: 'twitter:title', content: data.name });}
                if (data.excerpt) {this.meta.addTag({ name: 'twitter:description', content: data.excerpt });}
                if (data.image) {this.meta.addTag({ name: 'twitter:image', content: data.image });}
              }),
              finalize(() => {
                this.isLoading = false;                
              }
            ))
        )
      );
  }

  public changeVersion(version = 0) {
    this.version_tab = version;
  }
}
moment.locale('es-ar');
