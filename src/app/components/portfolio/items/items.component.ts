import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { startWith, delay, finalize, take, tap, switchMap } from 'rxjs/operators';
import { Portfolio } from '@models/portfolio.model';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'portfolio-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})

export class PortfolioItemsComponent implements OnInit {

  isLoading:boolean = true;
  public rows$: Observable<Portfolio[]>;

  onActivate(e, outlet){
    outlet.scrollTop = 0;
  }

  constructor(
    private apiService:ApiService
  ) {}

  ngOnInit() {
    this.rows$ = this.apiService.get<Portfolio[]>('portfolio', {limit: 3})
      .pipe(
        delay(0),
        tap((res:Portfolio[]) => {
          //console.log('pipe result', res);
        }),
        finalize(() => (this.isLoading = false))
      );
  }
}
