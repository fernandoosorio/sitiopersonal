import { Component, OnInit, Inject, Optional, PLATFORM_ID } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import { RESPONSE, REQUEST } from '@nguniversal/express-engine/tokens';
import { isPlatformServer, Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Request, Response } from 'express';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Component({
  selector: 'fo-error404',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class Error404Component implements OnInit {
  people: any;

  constructor(
    @Optional() @Inject(REQUEST) private request: Request,
    @Optional() @Inject(RESPONSE) private response: Response,
    @Inject(PLATFORM_ID) private platformId: any,
    private http: HttpClient,  
    meta: Meta, 
    title: Title,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {
    title.setTitle('Error 404 - ' + environment.site_name);
  }

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      this.response.status(404);
    };
    const replaceUrl = this.activatedRoute.snapshot.queryParams['url'];
    if (replaceUrl) {
      this.location.replaceState(replaceUrl);
    }
  }
}
