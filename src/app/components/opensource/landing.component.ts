import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import { environment } from '@environments/environment';

@Component({
  selector: 'FM5-opensource',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class OpenSourceComponent implements OnInit {

	constructor(
		meta: Meta, 
		title: Title
	) {
  		title.setTitle('Open Source - ' + environment.site_name);
		meta.addTags([
			{ name: 'keywords', content: 'fernando, osorio, open source, buenos aires, argentina'},
			{ name: 'description', content: 'El desarrollo de este sitio es de código abierto, y también posee la licencia Creative Commons.' },
			{ name: 'og:title', content: 'Open Source' },
			{ name: 'og:description', content: 'El desarrollo de este sitio es de código abierto, y también posee la licencia Creative Commons.' },
			{ name: 'twitter:title', content: 'Open Source' },
			{ name: 'twitter:description', content: 'El desarrollo de este sitio es de código abierto, y también posee la licencia Creative Commons.' }
		]);
	}

  ngOnInit() {
  }

}
