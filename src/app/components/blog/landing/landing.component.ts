import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import { Observable } from 'rxjs';
import { startWith, delay, finalize, take, tap, switchMap } from 'rxjs/operators';
import { ApiService } from '@services/api.service';
import { BlogPost } from '@models/blog-post.model';
import { environment } from '@environments/environment';

@Component({
  selector: 'fm5-blog',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class BlogLandingComponent implements OnInit {

  public isLoading:boolean = true;
  public posts$:Observable<BlogPost[]>;

  public section = {
    title: "Blog",
    cover_title: "Blog",
    description: 'Un rincón de opinión propia en Internet.'
  }

  constructor(
    meta: Meta, 
    title: Title, 
    private apiService: ApiService
  ) {

    meta.removeTag('name="keywords"');
    meta.removeTag('name="description"');
    meta.removeTag('name="og:title"');
    meta.removeTag('name="og:description"');
    meta.removeTag('name="twitter:title"');
    meta.removeTag('name="twitter:description"');

    title.setTitle('Blog - ' + environment.site_name);

    meta.addTags([
      { name: 'author',   content: 'Fernando Osorio'},
      { name: 'keywords', content: 'fernando, osorio, portfolio, frontend, buenos aires, argentina'},
      { name: 'description', content: 'Un rincón de opinión propia en Internet.' },
      { name: 'og:title', content: 'Blog' },
      { name: 'og:description', content: 'Un rincón de opinión propia en Internet.' },
      { name: 'twitter:title', content: 'Blog' },
      { name: 'twitter:description', content: 'Un rincón de opinión propia en Internet.' }
    ]);
  }

  ngOnInit() {
    this.posts$ = this.apiService.get<BlogPost[]>('blog')
      .pipe(
        delay(0),
        tap((res:BlogPost[]) => {
          //console.log('pipe result', res);
        }),
        finalize(() => (this.isLoading = false))
      );
  }
}
