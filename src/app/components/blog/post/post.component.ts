import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Meta, Title, DomSanitizer } from "@angular/platform-browser";
import { ApiService } from '@services/api.service';
import { Observable } from 'rxjs';
import { startWith, delay, finalize, take, tap, switchMap } from 'rxjs/operators';
import { BlogPost } from '@models/blog-post.model';
import * as moment from 'moment';
//import * as locales from 'moment/min/locales';
import { environment } from '@environments/environment';

@Component({
  selector: 'fm5-blog-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class BlogPostComponent implements OnInit {
  public slug;
  public post_date: any;
  public post_content: any;
  public vote_not_available: boolean = false;
  public vote_result;

  isLoading: boolean = true;
  public post$: Observable<BlogPost>;

  constructor(
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,
    private meta: Meta, 
    private title: Title,
    private domsanitizer: DomSanitizer
  ) {
  }

  ngOnInit(): void {
    this.post$ = this.activatedRoute.params.pipe(
        switchMap((data) =>
          this.apiService
            .get<BlogPost>(`blog`, {slug: data.slug})
            .pipe(
              delay(0),
              tap((post) => {

                if (post.date) {this.post_date = moment(post.date, moment.ISO_8601).format("dddd d [de] MMMM [de] YYYY[, a las] hh:mma");}
                if (post.content) {this.post_content = this.domsanitizer.bypassSecurityTrustHtml(post.content);}

                this.meta.removeTag('name="keywords"');
                this.meta.removeTag('name="description"');
                this.meta.removeTag('name="og:title"');
                this.meta.removeTag('name="og:description"');
                this.meta.removeTag('name="twitter:title"');
                this.meta.removeTag('name="twitter:description"');

                if (post.title) {this.title.setTitle(post.title + ' - ' + environment.site_name);}
                if (post.title) {this.meta.addTag({ name: 'og:title', content: post.title });}
                if (post.title) {this.meta.addTag({ name: 'twitter:title', content: post.title });}
                this.meta.addTags([
                  { name: 'author', content: 'Fernando Osorio'},
                  { name: 'keywords', content: 'fernando, osorio, blog, frontend, buenos aires, argentina'}
                ]);
                if (post.excerpt) {this.meta.addTag({ name: 'description', content: post.excerpt });}
                if (post.excerpt) {this.meta.addTag({ name: 'og:description', content: post.excerpt });}
                if (post.excerpt) {this.meta.addTag({ name: 'twitter:description', content: post.excerpt });}
              }),
              finalize(() => {
                this.isLoading = false;                
              }
            ))
        )
      );
  }

  onVote(option: string): void{
    if (this.vote_not_available) {
      this.vote_not_available = true;
      /*switch (option) {
        case "Y":
          this.apiService.blogVote(this.post.slug, 1).subscribe(result => {
            this.vote_result = result;
          });
          break;
        case "K":
          this.apiService.blogVote(this.post.slug, 2).subscribe(result => {
            this.vote_result = result;
          });
          break;
        case "N":
          this.apiService.blogVote(this.post.slug, 3).subscribe(result => {
            this.vote_result = result;
          });
          break;
      }*/
    };
  }
}

moment.locale('es-ar');
