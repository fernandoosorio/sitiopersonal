import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { delay, finalize, tap } from 'rxjs/operators';
import { Portfolio } from '@models/portfolio.model';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'fo-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  public activeLang = 'es';
  public age = 0;
  public textbirthday = '';

  isLoadingPortfolio:boolean = true;
  public portfolio$: Observable<Portfolio[]>;

  constructor(
    meta: Meta,
    title: Title,
    private apiService: ApiService,
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang(this.activeLang);
    title.setTitle('Fernando Osorio');
  }

  ngOnInit() {
    this.loadBirthday();
    this.getPortfolio();
  }

  public loadBirthday() {
    const d = new Date();
    const year = d.getFullYear();
    const birth = moment("1994-11-05");
    const birthday = moment(year+"-11-05");
    const birthday_next = moment((year+1)+"-11-05");
    const todaysdate = moment();
    this.age = todaysdate.diff(birth, 'years');
    const nextbirthday = birthday.diff(todaysdate, 'days')+1;
    const secondnextbirthday = birthday_next.diff(todaysdate, 'days')+1;
    const nextage = todaysdate.diff(birth, 'years')+1;

    let text = {
      today: 'Hoy es mi cumple!',
      tomorrow_pre: 'Mañana cumplo ',
      tomorrow_post: ' años',
      in: 'En ',
      days: ' días cumplo '
    };
    if (this.activeLang == 'en') {
      text = {
        today: 'Today is my birthday!',
        tomorrow_pre: 'Tomorrow I\'ll have ',
        tomorrow_post: ' years old',
        in: 'In ',
        days: ' days I\'ll have '
      };
    };

    if (birthday.diff(todaysdate, 'days')+1 === 0) {
      this.textbirthday = text.today;
    } else if (birthday.diff(todaysdate, 'days')+1 === 1) {
      this.textbirthday = text.tomorrow_pre + nextage + text.tomorrow_post;
    } else if (birthday.diff(todaysdate, 'days')+1 > 0) {
      this.textbirthday = text.in + nextbirthday + text.days + nextage;
    } else {
      this.textbirthday = text.in + secondnextbirthday + text.days + nextage;
    }
  }

  public getPortfolio() {
    this.portfolio$ = this.apiService.get<Portfolio[]>('portfolio', {limit: 3})
      .pipe(
        delay(0),
        tap((res:Portfolio[]) => {
          //console.log('pipe result', res);
        }),
        finalize(() => (this.isLoadingPortfolio = false))
      );
  }
}
