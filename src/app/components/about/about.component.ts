import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import * as moment from 'moment';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

	age: number = 0

	constructor(
		meta: Meta, 
		title: Title
	) {
		title.setTitle('Sobre mí - ' + environment.site_name);

		meta.removeTag('name="keywords"');
		meta.removeTag('name="description"');
		meta.removeTag('name="og:title"');
		meta.removeTag('name="og:description"');
		meta.removeTag('name="twitter:title"');
		meta.removeTag('name="twitter:description"');
		
		meta.addTags([
			{ name: 'keywords', content: 'fernando, osorio, hobbies, actividades, gustos, buenos aires, argentina'},
			{ name: 'description', content: 'Conocé un poco más de lo que hago en el día a día.' },
			{ name: 'og:title', content: 'Sobre mí' },
			{ name: 'og:description', content: 'Conocé un poco más de lo que hago en el día a día.' },
			{ name: 'twitter:title', content: 'Sobre mí' },
			{ name: 'twitter:description', content: 'Conocé un poco más de lo que hago en el día a día.' }
		]);
	}

	ngOnInit() {
		const birth = moment("1994-11-05");
		const todaysdate = moment();
		this.age = todaysdate.diff(birth, 'years');
	}
}