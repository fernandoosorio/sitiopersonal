import { PortfolioTechnology } from './portfolio-technology.model';

export class PortfolioVersion {
  display_name: string;
  name: string;
  version: number;
  excerpt: string;
  description: string;
  date_from?: string;
  date_to?: string;
  visible?: boolean;
  technologies?: PortfolioTechnology[];
  images?: string[];
}
