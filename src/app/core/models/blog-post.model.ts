import { Vote } from './vote.model';

export class BlogPost {
  title: string;
  slug: string;
  excerpt: string;
  content: string;
  date: string;
  vote: Vote;
  visible: boolean;
}
