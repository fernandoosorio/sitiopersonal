export class Notification {
	title: string;
	text: string;
	visible?: boolean;
	timeout?: number;
}
