import { PortfolioVersion } from './portfolio-version.model';

export class Portfolio {
  name: string;
  slug: string;
  excerpt: string;
  content: string;
  image: string;
  visible: boolean;
  versions?: PortfolioVersion[];
}
