export class JumbotronLinks {
  text: string;
  path: string;
  internal: boolean;
  visible: boolean;
}
