import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { NotificationService } from '@services/notification.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private notificationService: NotificationService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 0) {
          this.notificationService.trigger({
            title: 'Atención',
            text: 'Se perdió la conexión a Internet. Intentalo de nuevo más tarde.'
          });
        } else if (err.status === 403) {
          this.notificationService.trigger({
            title: 'Atención',
            text: 'No tenés permisos para acceder a esta sección.'
          });
        }

        return throwError(err);
      })
    );
  }
}
