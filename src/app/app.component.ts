import { Component, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { Notification } from '@models/notification.model';
import { NotificationService } from '@services/notification.service';
import {
	trigger,
	state,
	style,
	animate,
	transition
} from '@angular/animations';
import { slideInAnimation } from './animations';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	animations: [
		slideInAnimation
	]
})
export class AppComponent {

	isBrowser: boolean;
	theme: 'night';
	home: any = null;
	navbarCollapsed = true;
	public activeLang = 'es';
	public navbarScrolled = false;

	public notification:Notification;
	
	constructor(
		private notificationService: NotificationService,
		private translate: TranslateService,
		private cookieService: CookieService,
		@Inject(DOCUMENT) private document: Document,
		@Inject(PLATFORM_ID) private platformId
	) {
		this.isBrowser = isPlatformBrowser(this.platformId);
		this.translate.setDefaultLang(this.activeLang);

		this.notificationService.notificationObs.subscribe(
			(data:Notification) => {
				this.notification = data;
			}
		);
	}

	@HostListener('window:scroll', [])
	onWindowScroll() {
		if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
			this.navbarScrolled = true;
		} else {
			this.navbarScrolled = false;
		}
	}

	prepareRoute(outlet: RouterOutlet) {
		return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
	}

	onActivate(event) {
		this.notificationService.trigger(null);

		this.navbarCollapsed = true;
		if(this.isBrowser) {
			window.scrollTo({ top: 0, behavior: 'smooth' });
		}
	}

	public changeLang(code) {
		this.translate.use(code);
		this.activeLang = code;
	}
}