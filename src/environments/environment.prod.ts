export const environment = {
  production: true,
  site_name: 'Fernando Osorio',
  apiUri: 'https://api.fernandoosor.io/website/v1/'
};
