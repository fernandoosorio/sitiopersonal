# Sitio Personal
El repositorio donde publico las distintas versiones de mi página personal. 

## ¿Qué se usó?
* Angular-CLI
* Angular
* Node

## ¿Cómo verlo?
* Clonar/Bajar proyecto. 
* En el directorio, abrir la consola y ejecutar npm install (5-10 minutos aprox.). 
* Configurar archivos restantes. 
* npm start - El sitio se verá en localhost:3000
* Para verlo en modo producción, correr `set NODE_ENV=production`

## Changelog
Podrás encontrar todos los cambios realizados [acá](https://bitbucket.org/fernandoosorio/sitiopersonal/src/master/CHANGELOG.md).

## Desarrollo contínuo y abierto
El sitio se continuará actualizando y también podés cooperar a mejorarlo. Todo lo que ves en el repositorio lo podrás ver en [fernandoosor.io](https://fernandoosor.io).